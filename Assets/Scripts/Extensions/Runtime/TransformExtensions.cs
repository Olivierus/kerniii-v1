﻿using UnityEngine;

namespace DutchSkull.Utilities
{
    public static class TransformExtensions
    {
        public static Vector2 PostitionXZ(this Transform transform) => new Vector2(transform.position.x, transform.position.z);
    }
}