﻿using UnityEngine;

public static class AStarMapGenerator
{
#if UNITY_EDITOR
    private static readonly RaycastHit[] RESULT = new RaycastHit[1];
    private static OneDArray map;

    public static OneDArray Generate(Vector2Int size)
    {
        map = new OneDArray(size);

        for (int i = 0; i < map.data.Length; i++)
            SetMapTileValue(map.GetX(i), map.GetY(i));

        return map;
    }


    private static void SetMapTileValue(int x, int y)
    {
        switch (PositionHasObstacle(new Vector3(x, 0, y)))
        {
            case true:
                map.data[map.GetIndex(y, x)] = -1;
                break;
            case false:
                map.data[map.GetIndex(y, x)] = 0;
                break;
        }
    }


    private static bool PositionHasObstacle(Vector3 vector3)
    {
        vector3 -= Vector3.up;

        Ray ray = new Ray(vector3, Vector3.up);

        int hits = Physics.RaycastNonAlloc(ray, RESULT, Mathf.Infinity);

        if (hits == 0)
            return false;

        if (RESULT[0].transform.gameObject.isStatic)
            return true;
        else
            return true;
    }
#endif
}
