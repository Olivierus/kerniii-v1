﻿using UnityEngine;

namespace DutchSkull.Astar
{
    public class Node
    {
        public Vector2 Position { get => new Vector2(X, Y); }
        public int X { get; set; }
        public int Y { get; set; }
        public double FScore { get; set; }
        public double GScore { get; set; }
        public int V { get; set; }
        public Node Parent { get; set; }

        public Node(int x, int y) => (X, Y) = (x, y);
    }
}