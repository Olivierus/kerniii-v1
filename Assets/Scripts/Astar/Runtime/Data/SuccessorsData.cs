﻿namespace DutchSkull.Astar
{
    public struct SuccessorsData
    {
        public bool xNorth;
        public bool xSouth;
        public bool xEast;
        public bool xWest;

        public int North;
        public int South;
        public int East;
        public int West;

        public AStarMapData mapData;
        public int[] grid;

        public int rows;
        public int cols;

        public Node[] result;

        public int i;

        public SuccessorsData(AStarMapData mapData, int rows, int cols, Node node)
        {
            this.mapData = mapData;

            this.grid = mapData.map.data;
            this.rows = rows;
            this.cols = cols;

            North = node.Y - 1;
            South = node.Y + 1;
            East = node.X + 1;
            West = node.X - 1;

            xNorth = North > -1 && this.grid[mapData.map.GetIndex(North, node.X)] == 0;
            xSouth = South < this.rows && this.grid[mapData.map.GetIndex(South, node.X)] == 0;
            xEast = East < this.cols && this.grid[mapData.map.GetIndex(node.Y, East)] == 0;
            xWest = West > -1 && this.grid[mapData.map.GetIndex(node.Y, West)] == 0;

            i = 0;

            result = new Node[8];

            if (xNorth)
                result[i++] = new Node(node.X, North);

            if (xEast)
                result[i++] = new Node(East, node.Y);

            if (xSouth)
                result[i++] = new Node(node.X, South);

            if (xWest)
                result[i++] = new Node(West, node.Y);
        }
    }
}