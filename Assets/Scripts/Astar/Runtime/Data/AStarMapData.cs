﻿using UnityEngine;

public class AStarMapData : ScriptableObject
{
    [SerializeField] public OneDArray map;
    [SerializeField] public Vector2Int size;
}
