﻿using System;
using UnityEngine;

[Serializable]
public class OneDArray
{
    [SerializeField] public int[] data;
    [SerializeField] public Vector2Int size;

    public OneDArray(Vector2Int size)
    {
        data = new int[size.x * size.y];
        this.size = size;
    }

    public int GetX(int index) => index % size.x;

    public int GetY(int index) => (index / size.x) % size.y;

    public int GetIndex(int x, int y) => x + y * size.x;
}
