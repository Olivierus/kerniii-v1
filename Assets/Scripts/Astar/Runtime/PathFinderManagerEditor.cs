﻿using DutchSkull.Astar;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

[ExecuteAlways]
public class PathFinderManagerEditor : MonoBehaviour
{
#if UNITY_EDITOR
    [SerializeField] private Vector2Int size = new Vector2Int(25, 25);
    private Vector2Int Size => size;

    private AStarMapData mapData;

    private void Awake() => InnitializeMapData();

    private void Start() => InnitializeMapData();

    private void OnValidate() => InnitializeMapData();

    private bool InnitializeMapData()
    {
        if (!enabled)
            return false;

        if (size.x <= 0 || size.y <= 0)
            return Log.Warning($"{nameof(PathFinderManagerEditor)}: The map size can't be negative or 0. Please check.", false);

        if (mapData == null)
            mapData = LoadMapData();

        if (mapData == null)
            mapData = CreateMapData();

        mapData.size = size;
        mapData.map = AStarMapGenerator.Generate(Size);

        SaveMapData();

        return true;
    }

    private AStarMapData CreateMapData()
    {
        AssetDatabase.CreateFolder(Astar.ROOT_FOLDER_PATH, Astar.RESOURCE_FOLDER_NAME);

        AssetDatabase.CreateAsset(mapData, $"{Astar.ROOT_FOLDER_PATH}/{Astar.RESOURCE_FOLDER_NAME}/{SceneManager.GetActiveScene().name}_{Astar.MAP_DATA_IDENTIFIER}.Asset");

        return ScriptableObject.CreateInstance<AStarMapData>();
    }

    private void SaveMapData()
    {
        EditorUtility.SetDirty(mapData);

        AssetDatabase.SaveAssets();

        AssetDatabase.Refresh();
    }

    private AStarMapData LoadMapData()
    {
        AStarMapData mapData = Resources.Load<AStarMapData>($"{SceneManager.GetActiveScene().name}_{Astar.MAP_DATA_IDENTIFIER}");

        bool hasSceneName = mapData.name.Contains(SceneManager.GetActiveScene().name);

        return hasSceneName ? mapData : Log.Message<AStarMapData>("No map data for current scene found.");
    }

    private void OnDrawGizmos()
    {
        if (!enabled)
            return;

        for (int x = 0; x < Size.x; x++)
            for (int y = 0; y < Size.y; y++)
                DrawNode(x, y);
    }

    private void DrawNode(int x, int y)
    {
        Vector3 one = Vector3.one;
        one.y = 0;

        int nodeValue = mapData.map.data[mapData.map.GetIndex(y, x)];
        Gizmos.color = nodeValue == 0 ? Color.white : Color.red;

        Gizmos.DrawWireCube(new Vector3(x, 0, y), one);
    }
#endif
}
