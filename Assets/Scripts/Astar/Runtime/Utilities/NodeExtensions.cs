﻿namespace DutchSkull.Astar
{
    public static class NodeExtensions
    {
        internal static Node[] Successors(this Node node, AStarMapData mapData, int rows, int cols, HeuristicMode heuristic)
        {
            SuccessorsData data = new SuccessorsData(mapData, rows, cols, node);

            if (heuristic == HeuristicMode.Diagonal || heuristic == HeuristicMode.Euclidean)
            {
                if (heuristic == HeuristicMode.Manhattan)
                    return DiagonalSuccessorsFree(data);
                else
                    return DiagonalSuccessors(data);
            }

            return data.result;
        }

        private static Node[] DiagonalSuccessors(SuccessorsData data)
        {
            if (data.xNorth)
            {
                if (data.xEast && data.grid[data.mapData.map.GetIndex(data.North, data.East)] == 0)
                    data.result[data.i++] = new Node(data.East, data.North);

                if (data.xWest && data.grid[data.mapData.map.GetIndex(data.North, data.West)] == 0)
                    data.result[data.i++] = new Node(data.West, data.North);
            }

            if (data.xSouth)
            {
                if (data.xEast && data.grid[data.mapData.map.GetIndex(data.South, data.East)] == 0)
                    data.result[data.i++] = new Node(data.East, data.South);

                if (data.xWest && data.grid[data.mapData.map.GetIndex(data.South, data.West)] == 0)
                    data.result[data.i++] = new Node(data.West, data.South);
            }

            return data.result;
        }

        private static Node[] DiagonalSuccessorsFree(SuccessorsData data)
        {
            data.xNorth = data.North > -1;
            data.xSouth = data.South < data.rows;
            data.xEast = data.East < data.cols;
            data.xWest = data.West > -1;

            if (data.xEast)
            {
                if (data.xNorth && data.grid[data.mapData.map.GetIndex(data.North, data.East)] == 0)
                    data.result[data.i++] = new Node(data.East, data.North);

                if (data.xSouth && data.grid[data.mapData.map.GetIndex(data.South, data.East)] == 0)
                    data.result[data.i++] = new Node(data.East, data.South);
            }
            if (data.xWest)
            {
                if (data.xNorth && data.grid[data.mapData.map.GetIndex(data.North, data.West)] == 0)
                    data.result[data.i++] = new Node(data.West, data.North);

                if (data.xSouth && data.grid[data.mapData.map.GetIndex(data.South, data.West)] == 0)
                    data.result[data.i++] = new Node(data.West, data.South);
            }
            return data.result;
        }
    }
}