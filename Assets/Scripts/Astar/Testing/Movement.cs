﻿using DutchSkull.Utilities;
using DutchSkull.Gizmos;
using System.Collections.Generic;
using UnityEngine;
using DutchSkull.Astar;

public class Movement : MonoBehaviour
{
    [SerializeField] private float moveSpeed = 1f;
    [SerializeField] private float rotationSpeed = 1f;

    [SerializeField] private Vector2 goal = new Vector2(1, 1);

    private List<Vector2> path = new List<Vector2>();

    private int currentNodeIndex = 0;

    private bool HasArrivedAtPathEnd => path == null || currentNodeIndex >= path.Count;

    private void Start() => GetRandomPath();

    private void Update()
    {
        if (HasArrivedAtPathEnd)
            GetRandomPath();

        MoveOnPath(path);
    }

    private bool GetPath() => Astar.GetPath(transform.PostitionXZ(), goal, out path);

    private void GetRandomPath()
    {
        currentNodeIndex = 0;

        goal = Vector2Utilities.Random(Vector2.zero, Astar.Size);

        bool hasFoundPath = GetPath();

        if (hasFoundPath)
            return;

        GetRandomPath();
    }

    private void MoveOnPath(List<Vector2> path)
    {
        if (currentNodeIndex > path.Count)
            return;

        Vector3 position = new Vector3(path[currentNodeIndex].x, 0, path[currentNodeIndex].y);

        transform.position = Vector3.MoveTowards(transform.position, position, moveSpeed * Time.deltaTime);

        Vector3 direction = position - transform.position;

        if (direction != Vector3.zero)
        {
            direction.y = 0;
            direction = direction.normalized;
            Quaternion rotation = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, (moveSpeed * rotationSpeed) * Time.deltaTime);
        }

        if (Vector3.Distance(position, transform.position) > 0)
            return;

        currentNodeIndex++;
    }

    private void OnDrawGizmos()
    {
        if (!enabled &&
            !Application.isPlaying)
            return;

        DrawGizmos.DrawBeginAndEndPoint(transform.PostitionXZ(), goal);

        bool hasFoundPath = GetPath();

        if (!hasFoundPath)
            return;

        DrawGizmos.DrawPath(path);
    }

    private Astar Astar => Astar.Instance;
}
